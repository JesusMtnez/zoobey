package es.jesusmtnez

final case class Config(
    telegram: TelegramConf
)

final case class TelegramConf(token: String)
