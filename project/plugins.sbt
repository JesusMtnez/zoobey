addSbtPlugin("org.scalameta"             % "sbt-scalafmt"        % "2.3.0")
addSbtPlugin("io.github.davidgregory084" % "sbt-tpolecat"        % "0.1.10")
addSbtPlugin("org.lyranthe.sbt"          % "partial-unification" % "1.1.2")
