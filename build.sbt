val scalaV = "2.13.1"

val betterMonadicForV = "0.3.1"
val contextAppliedV = "0.1.2"
val kindProjectorV = "0.11.0"

val canoeV = "0.4.0"
val catsV = "2.1.0"
val catsEffectV = "2.0.0"
val fs2V = "2.2.1"
val pureconfigV = "0.12.2"

lazy val root = project
  .in(file("."))
  .settings(commonSettings)
  .settings(
    name := "Zoobey"
  )

lazy val commonSettings = Seq(
  organization := "es.jesusmtnez",
  scalaVersion := scalaV,
  cancelable in Scope.Global := true,
  addCompilerPlugin("org.typelevel"  % "kind-projector"      % kindProjectorV cross CrossVersion.full),
  addCompilerPlugin("com.olegpy"     %% "better-monadic-for" % betterMonadicForV),
  addCompilerPlugin("org.augustjune" %% "context-applied"    % contextAppliedV),
  libraryDependencies ++= Seq(
    "org.typelevel"         %% "cats-core"              % catsV,
    "org.typelevel"         %% "cats-effect"            % catsEffectV,
    "co.fs2"                %% "fs2-io"                 % fs2V,
    "com.github.pureconfig" %% "pureconfig"             % pureconfigV,
    "com.github.pureconfig" %% "pureconfig-cats-effect" % pureconfigV,
    "org.augustjune"        %% "canoe"                  % canoeV
  )
)
