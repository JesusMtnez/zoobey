package es.jesusmtnez

import canoe.api._
import canoe.models.Update
import canoe.syntax._
import cats.effect._
import fs2.Stream

object Zoobey {

  def start[F[_]: ConcurrentEffect](conf: TelegramConf): Stream[F, Update] = {
    Stream
      .resource(TelegramClient.global(conf.token))
      .flatMap { implicit client =>
        Bot.polling[F].follow(allOk)
      }
  }

  private def allOk[F[_]: TelegramClient]: Scenario[F, Unit] = {
    for {
      msg <- Scenario.expect(any)
      _ <- Scenario.eval(msg.chat.send("OK!"))
    } yield ()
  }

}
