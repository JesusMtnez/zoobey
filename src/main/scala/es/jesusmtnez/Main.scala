package es.jesusmtnez

import canoe.models.Update
import cats.effect._
import cats.implicits._
import fs2.Stream
import pureconfig._
import pureconfig.generic.auto._
import pureconfig.module.catseffect.syntax._

object Main extends IOApp {

  override def run(args: List[String]): IO[ExitCode] =
    ZoobeyApp.start[IO].compile.drain.as(ExitCode.Success)
}

object ZoobeyApp {

  def start[F[_]: ConcurrentEffect]: Stream[F, Update] = {
    for {
      conf <- Stream.eval(ConfigSource.default.at("zoobey").loadF[F, Config])
      app <- Zoobey.start[F](conf.telegram)
    } yield app
  }
}
